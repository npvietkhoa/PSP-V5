#ifndef V3_OS_MEMHEAP_DRIVERS_H
#define V3_OS_MEMHEAP_DRIVERS_H

#include "os_mem_drivers.h"
#include <stddef.h>
#include <avr/pgmspace.h>
#include "defines.h"

//----------------------------------------------------------------------------
// Macros
//----------------------------------------------------------------------------
#ifndef intHeap
#define intHeap (&intHeap__)
#endif

#ifndef extHeap
#define extHeap (&extHeap__)
#endif

//! All available heap allocation strategies.
typedef enum {
    OS_MEM_FIRST,
    OS_MEM_NEXT,
    OS_MEM_BEST,
    OS_MEM_WORST
} AllocStrategy;

typedef struct {
    MemDriver *driver;
    MemAddr mapStartingAddress;
    size_t mapSize;
    MemAddr useStartingAddress;
    size_t useSize;
    AllocStrategy currentAllocationStrategy;
    const char *name;
    MemAddr allocFrameStart[MAX_NUMBER_OF_PROCESSES];
    MemAddr allocFrameEnd[MAX_NUMBER_OF_PROCESSES];
} Heap;

//----------------------------------------------------------------------------
// Function headers
//----------------------------------------------------------------------------

//! Initialises all Heaps.
void os_initHeaps(void);

//! Needed for Taskmanager interaction.
Heap *os_lookupHeap(uint8_t index);

//! Needed for Taskmanager interaction.
size_t os_getHeapListLength(void);

Heap intHeap__;

Heap extHeap__;

#endif //V3_OS_MEMHEAP_DRIVERS_H
