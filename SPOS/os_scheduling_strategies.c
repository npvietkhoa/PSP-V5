#include "os_scheduling_strategies.h"
#include "defines.h"

#include <stdlib.h>

//----------------------------------------------------------------------------
// Globals
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Function definitions
//----------------------------------------------------------------------------

/*!
 *  Reset the scheduling information for a specific strategy
 *  This is only relevant for RoundRobin and InactiveAging
 *  and is done when the strategy is changed through os_setSchedulingStrategy
 *
 * \param strategy  The strategy to reset information for
 */
void os_resetSchedulingInformation(SchedulingStrategy strategy) {
    // This is a presence task

    if (strategy == OS_SS_ROUND_ROBIN) {
        schedulingInfo.timeSlice = os_getProcessSlot(
                os_getCurrentProc())->priority; // for round-robin timeSlice = priority
        return; // terminate this method immediately
    }

    if (strategy == OS_SS_INACTIVE_AGING) {
        for (uint8_t i = 0; i < MAX_NUMBER_OF_PROCESSES; ++i) {
            schedulingInfo.age[i] = 0; // with inactive aging set all the routine of age to 0
        }
        return;
    }

    if (strategy == OS_SS_MULTI_LEVEL_FEEDBACK_QUEUE) {
        for (int i = 0; i < MAX_NUMBER_OF_QUEUES; i++) {
            pqueue_reset(MLFQ_getQueue(i));
        }

        for (int id = 1; id < MAX_NUMBER_OF_PROCESSES; id++) {
            if (os_getProcessSlot(id)->state != OS_PS_UNUSED) {
				MLFQ_removePID(id);
                uint8_t queueId = MLFQ_MapToQueue(os_getProcessSlot(id)->priority);
                pqueue_append(MLFQ_getQueue(queueId), id);
                schedulingInfo.indvidualTimeSlot[id] = MLFQ_getDefaultTimeslice(queueId);

            }
        }
    }
}

/*!
 *  Reset the scheduling information for a specific process slot
 *  This is necessary when a new process is started to clear out any
 *  leftover data from a process that previously occupied that slot
 *
 *  \param id  The process slot to erase state for
 */
void os_resetProcessSchedulingInformation(ProcessID id) {
    // This is a presence task
    schedulingInfo.age[id] = 0;

    MLFQ_removePID(id); // delete old data of pid in queues
    uint8_t queueId = MLFQ_MapToQueue(os_getProcessSlot(id)->priority);
    //Füge den Prozess in der richtigen Warteschlange ein
    pqueue_append(MLFQ_getQueue(queueId), id);
    schedulingInfo.indvidualTimeSlot[id] = MLFQ_getDefaultTimeslice(queueId);
}

/*!
 *  This function implements the even strategy. Every process gets the same
 *  amount of processing time and is rescheduled after each scheduler call
 *  if there are other processes running other than the idle process.
 *  The idle process is executed if no other process is ready for execution
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed determined on the basis of the even strategy.
 */
ProcessID os_Scheduler_Even(Process const processes[], ProcessID current) {
    //#warning IMPLEMENT STH. HERE
    ProcessID procId = current;
    do {
        procId += 1;
        if (procId >= MAX_NUMBER_OF_PROCESSES && current == 0) {
            for (ProcessID pid = 0; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
                if (processes[pid].state == OS_PS_BLOCKED) return pid;
            }
            return 0;
        }
        if (procId >= MAX_NUMBER_OF_PROCESSES) procId = 1;
        if (processes[procId].state == OS_PS_READY) {
            for (ProcessID pid = 0; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
                if (processes[pid].state == OS_PS_BLOCKED) os_getProcessSlot(pid)->state = OS_PS_READY;
            }
            return procId;
        }
    } while (procId != current);

    for (ProcessID pid = 0; pid < MAX_NUMBER_OF_PROCESSES; pid++) if (processes[pid].state == OS_PS_BLOCKED) return pid;
    return 0;
}


/*!
 *  This function implements the random strategy. The next process is chosen based on
 *  the result of a pseudo random number generator.
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed determined on the basis of the random strategy.
 */
ProcessID os_Scheduler_Random(Process const processes[], ProcessID current) {
    //#warning IMPLEMENT STH. HERE

    // Get the number of processes, which are currently ready and blocked
    uint8_t numberOfActiveProcs = 0;
    uint8_t numberOfBlockedProcs = 0;

    for (uint8_t pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY) numberOfActiveProcs++;
        if (processes[pid].state == OS_PS_BLOCKED) numberOfBlockedProcs++;
    }
    if (numberOfActiveProcs == 0 && numberOfBlockedProcs != 0) {
        for (ProcessID pid = 0; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
            if (processes[pid].state == OS_PS_BLOCKED) os_getProcessSlot(pid)->state = OS_PS_READY;
        }
        numberOfActiveProcs = numberOfBlockedProcs;
    }

    int randomNumber = rand() % numberOfActiveProcs; // generate random number for picking up the next process

    ProcessID procId = 0;
    do {
        procId += 1;
        if (procId >= MAX_NUMBER_OF_PROCESSES) return 0;
        if (processes[procId].state == OS_PS_READY) randomNumber -= 1;
    } while (randomNumber >= 0);

    for (ProcessID pid = 0; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_BLOCKED) os_getProcessSlot(pid)->state = OS_PS_READY;
    }
    return procId;
}

/*!
 *  This function implements the round-robin strategy. In this strategy, process priorities
 *  are considered when choosing the next process. A process stays active as long its time slice
 *  does not reach zero. This time slice is initialized with the priority of each specific process
 *  and decremented each time this function is called. If the time slice reaches zero, the even
 *  strategy is used to determine the next process to run.
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed determined on the basis of the round robin strategy.
 */
ProcessID os_Scheduler_RoundRobin(Process const processes[], ProcessID current) {
    // Get the number of processes, which are currently ready and blocked
    uint8_t numberOfActiveProcs = 0;
    uint8_t numberOfBlockedProcs = 0;

    for (uint8_t pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY) numberOfActiveProcs++;
        if (processes[pid].state == OS_PS_BLOCKED) numberOfBlockedProcs++;
    }

    // If there are no ready procs then toggle every blocked ones to ready
    if (numberOfActiveProcs == 0 && numberOfBlockedProcs != 0) {
        for (ProcessID pid = 0; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
            if (processes[pid].state == OS_PS_BLOCKED) os_getProcessSlot(pid)->state = OS_PS_READY;
        }
        numberOfActiveProcs = numberOfBlockedProcs;
    }


    if (schedulingInfo.timeSlice > 1 && processes[current].state == OS_PS_READY) {
        schedulingInfo.timeSlice -= 1;
        for (int i = 0; i < MAX_NUMBER_OF_PROCESSES; i++) {
            if (processes[i].state == OS_PS_BLOCKED) os_getProcessSlot(i)->state = OS_PS_READY;
        }
        return current;
    }

    ProcessID nextPid = os_Scheduler_Even(processes, current);
    schedulingInfo.timeSlice = processes[nextPid].priority;

    for (int i = 0; i < MAX_NUMBER_OF_PROCESSES; i++) {
        if (processes[i].state == OS_PS_BLOCKED) os_getProcessSlot(i)->state = OS_PS_READY;
    }
    return nextPid;
}

/*!
 *  This function realizes the inactive-aging strategy. In this strategy a process specific integer ("the age") is used to determine
 *  which process will be chosen. At first, the age of every waiting process is increased by its priority. After that the oldest
 *  process is chosen. If the oldest process is not distinct, the one with the highest priority is chosen. If this is not distinct
 *  as well, the one with the lower ProcessID is chosen. Before actually returning the ProcessID, the age of the process who
 *  is to be returned is reset to its priority.
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed, determined based on the inactive-aging strategy.
 */
ProcessID os_Scheduler_InactiveAging(Process const processes[], ProcessID current) {
    // Get the number of processes, which are currently ready and blocked
    uint8_t numberOfActiveProcs = 0;
    uint8_t numberOfBlockedProcs = 0;

    for (uint8_t pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY) numberOfActiveProcs++;
        if (processes[pid].state == OS_PS_BLOCKED) numberOfBlockedProcs++;
    }

    // If there are no ready procs then toggle every blocked ones to ready
    if (numberOfActiveProcs == 0 && numberOfBlockedProcs != 0) {
        for (ProcessID pid = 0; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
            if (processes[pid].state == OS_PS_BLOCKED) os_getProcessSlot(pid)->state = OS_PS_READY;
        }
        numberOfActiveProcs = numberOfBlockedProcs;
    }

    Age maxAge = 0;
    for (ProcessID pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY) {
            schedulingInfo.age[pid] += processes[pid].priority; // Update ages for processes

            // find the max age simultaneously
            if (schedulingInfo.age[pid] > maxAge) {
                maxAge = schedulingInfo.age[pid];
            }
        }
    }

    // Loop once to find first proc with maxAge - cuz possibly more than one has the same age and priority
    ProcessID lowestPIDProc = 0;
    ProcessID highestPriorityProc = 0;

    for (ProcessID pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY && schedulingInfo.age[pid] == maxAge) {
            lowestPIDProc = pid;
            highestPriorityProc = pid;
            break;
        }
    }

    // Loop forward all the process array to find the process, which allowed performing next
    for (ProcessID pid = lowestPIDProc; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY &&
            processes[pid].priority > processes[highestPriorityProc].priority &&
            schedulingInfo.age[pid] == maxAge) {
            highestPriorityProc = pid;
        }
    }

    // toggle blocked procs to ready before return nextPid
    for (ProcessID pid = 0; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_BLOCKED) os_getProcessSlot(pid)->state = OS_PS_READY;
    }

    if (lowestPIDProc == highestPriorityProc) { // Just one process found or processes have the same priority and age
        schedulingInfo.age[lowestPIDProc] = 0; // set age back to 0
        return lowestPIDProc;
    } else {    // If there's more than one with same maxAge, then choose one which the highest priority
        schedulingInfo.age[highestPriorityProc] = 0; // set age back to 0
        return highestPriorityProc;
    }
}

/*!
 *  This function realizes the run-to-completion strategy.
 *  As long as the process that has run before is still ready, it is returned again.
 *  If  it is not ready, the even strategy is used to determine the process to be returned
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed, determined based on the run-to-completion strategy.
 */
ProcessID os_Scheduler_RunToCompletion(Process const processes[], ProcessID current) {
    // This is a presence task
    if (processes[current].state == OS_PS_READY) return current;
    return os_Scheduler_Even(processes, current);
}

/*!
 * Initializes the given ProcessQueue with a predefined size
 * @param queue The ProcessQueue to initialize.
 */
void pqueue_init(ProcessQueue *queue) {
    for (uint8_t i = 0; i < MAX_NUMBER_OF_PROCESSES; i++) (*queue).data[i] = 0;
    queue->size = 0;
    queue->tail = 0;
    queue->head = 0;
}

/*!
 * Resets the given ProcessQueue.
 * @param queue The ProcessQueue to reset.
 */
void pqueue_reset(ProcessQueue *queue) {
    for (uint8_t i = 0; i < MAX_NUMBER_OF_PROCESSES; i++) (*queue).data[i] = 0;
    queue->size = 0;
    queue->tail = 0;
    queue->head = 0;
}

/*!
 * Checks whether there is next a ProcessID.
 * @param queue The ProcessQueue to check.
 * @return 0 if there's no next a ProcessID
 */
uint8_t pqueue_hasNext(ProcessQueue *queue) {
    return queue->size != 0;
}

/*!
 * Returns the first ProcessID of the given ProcessQueue.
 * @param queue The	specific ProcessQueue.
 * @return the first ProcessID.
 */
ProcessID pqueue_getFirst(ProcessQueue *queue) {
    if (!pqueue_hasNext(queue)) os_errorPStr(PSTR("Queue empty"));
    return (*queue).data[queue->tail];
}

/*!
 * Drops the first ProcessID of the given ProcessQueue.
 * @param queue The	specific ProcessQueue.
 */
void pqueue_dropFirst(ProcessQueue *queue) {
    if (!pqueue_hasNext(queue)) os_errorPStr(PSTR("Queue empty"));
    (*queue).data[queue->tail] = 0;
    queue->size--;
    if (queue->tail != MAX_NUMBER_OF_PROCESSES - 1) {
        if (pqueue_hasNext(queue)) queue->tail++;
    } else {
        queue->tail = 0;
    }
}

/*!
 * Appends a ProcessID to the given ProcessQueue.
 * @param queue The ProcessQueue in which the pid should be appended.
 * @param pid The ProcessId to append.
 */
void pqueue_append(ProcessQueue *queue, ProcessID pid) {
    // check queue firstly
    if (queue->size >= MAX_NUMBER_OF_PROCESSES) {
        os_errorPStr(PSTR("Queue full"));
    }

    // Update head
    if (queue->head != MAX_NUMBER_OF_PROCESSES - 1) {
        if (pqueue_hasNext(queue)) queue->head++;
    } else {
        queue->head = 0;
    }

    // Update size
    queue->size++;

    //assign data to queue
    (*queue).data[queue->head] = pid;
}

/*!
 * Returns the corresponding ProcessQueue.
 * Returns a pointer to the ProcessQueue with index queueID from the schedulingInformation
 * @param queueID queueID	Index of the queue.
 * @return Pointer to the specific ProcessQueue.
 */
ProcessQueue *MLFQ_getQueue(uint8_t queueID) {
    return &(schedulingInfo.queue[queueID]);
}

void os_initSchedulingInformation() {
    for (int i = 0; i < MAX_NUMBER_OF_QUEUES; i++) {
        pqueue_init(MLFQ_getQueue(i));
    }
}

uint8_t MLFQ_getDefaultTimeslice(uint8_t queueID) {
    return (1 << queueID);
}


/*!
 * Removes a ProcessID from the given ProcessQueue.
 * @param queue The ProcessQueue from which the pid should be removed.
 * @param pid The ProcessId to remove.
 */
void pqueue_removePID(ProcessQueue* queue, ProcessID pid) {
    if (pqueue_hasNext(queue)){
        ProcessID pidInQueue;
        // get first in queue and append to last,
        // if pid is found, it will be just dropped and not be appended
        uint8_t queueSize = queue->size; // save locally cuz size will be changed
        for(uint8_t i = 0; i < queueSize; i++) {
            pidInQueue = pqueue_getFirst(queue);
            pqueue_dropFirst(queue);
            if(pidInQueue == pid) {
                continue;
            }
            pqueue_append(queue, pidInQueue);
        }
    }
}

/*!
 * Function that removes the given ProcessID from the ProcessQueues.
 * @param pid The ProcessId to remove.
 */
void MLFQ_removePID(ProcessID pid) {
    for (uint8_t queueId = 0; queueId < MAX_NUMBER_OF_QUEUES; queueId++) { // search up the queues to clean up pid
        ProcessQueue *currQueue = MLFQ_getQueue(queueId);
        pqueue_removePID(currQueue, pid);
    }
}


bool pqueue_containsPID(ProcessQueue *queue, ProcessID pid) {
    for (uint8_t i = 0; i < MAX_NUMBER_OF_PROCESSES; i++) {
        if (queue->data[i] == pid) return true;
    }
    return false;
}

uint8_t MLFQ_MapToQueue(Priority prio) {
    //shift 6 to the right to leave only the two MSBs of the priority
    uint8_t class = prio >> 6;
    return (MAX_NUMBER_OF_QUEUES - 1) - class;
}

ProcessID os_Scheduler_MLFQ(Process const processes[], ProcessID current) {
    // Get the number of processes, which are currently ready and blocked
    uint8_t numberOfActiveProcs = 0;
    uint8_t numberOfBlockedProcs = 0;

    for (uint8_t pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY) numberOfActiveProcs++;
        if (processes[pid].state == OS_PS_BLOCKED) numberOfBlockedProcs++;
    }

    // If there are no ready procs then toggle every blocked ones to ready
    if (numberOfActiveProcs == 0 && numberOfBlockedProcs != 0) {
        for (ProcessID pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
            if (processes[pid].state == OS_PS_BLOCKED) os_getProcessSlot(pid)->state = OS_PS_READY;
        }
    }
	

    ProcessID nextPidInQueue; // running variable to find next one
    bool foundReadyProc = false;
    uint8_t queueId = 0;
    // Searching for ready proc - nextPidInQueue and queue which contains this proc - currQueue
    for ( ;(queueId < MAX_NUMBER_OF_QUEUES); queueId++) {
        ProcessQueue *currQueue = MLFQ_getQueue(queueId);
        if (pqueue_hasNext(currQueue)) {  //Stelle fest, ob die Warteschlange nicht leer ist
            // search for ready process, if it exists, it will be moved up to first over blocked one
            uint8_t queueSize = currQueue->size; // save locally cuz size will be changed
            for (int i = 0; i < queueSize; i++) {
                nextPidInQueue = pqueue_getFirst(currQueue);
                if (processes[nextPidInQueue].state == OS_PS_READY) {
                    foundReadyProc = true;
                    break;
                }
                pqueue_dropFirst(currQueue);
                if (processes[nextPidInQueue].state != OS_PS_UNUSED) pqueue_append(currQueue, nextPidInQueue);
            }
        }
        if (foundReadyProc) break;
    }

    if(!foundReadyProc) return 0;

    // found Ready Procs then update time slice and its class if possible
	schedulingInfo.indvidualTimeSlot[nextPidInQueue] -= 1;
	if (schedulingInfo.indvidualTimeSlot[nextPidInQueue] == 0) {
		for (uint8_t qid = 0; qid < MAX_NUMBER_OF_QUEUES; qid++) { // search in each queue to find where exactly pid stands
			ProcessQueue* currQueue = MLFQ_getQueue(qid);
			if (pqueue_containsPID(currQueue, nextPidInQueue)) {
				pqueue_dropFirst(currQueue);
				uint8_t nextQueueId = (qid >= MAX_NUMBER_OF_QUEUES - 1) ? qid : (qid + 1);
				ProcessQueue* nextQueue = MLFQ_getQueue(nextQueueId);
				// move pid to the next queue
				pqueue_append(nextQueue, nextPidInQueue);
				schedulingInfo.indvidualTimeSlot[nextPidInQueue] = MLFQ_getDefaultTimeslice(nextQueueId);
				break;
			}
		}
	}

    // move all blocked pid to end of queue and toggle blocked procs to ready before return nextPid
	ProcessID blockedList[MAX_NUMBER_OF_PROCESSES];
	uint8_t blockedListIndex = 0;
	for (uint8_t i = 0; i < MAX_NUMBER_OF_QUEUES; i++) {
		ProcessQueue* queue = MLFQ_getQueue(i);
		uint8_t sizeOfQueue = queue->size;
		ProcessID tempPid;
        // for each queue we search for blocked ones and save into array
		for (uint8_t i = 0; i < sizeOfQueue; i++) {
			tempPid = pqueue_getFirst(queue);
			pqueue_dropFirst(queue);
			if (os_getProcessSlot(tempPid)->state == OS_PS_BLOCKED) { // blocked ones will be dropped and won't be added
				blockedList[blockedListIndex] = tempPid;
				blockedListIndex += 1;
				continue;
			}
			pqueue_append(queue, tempPid);
		}
		for (uint8_t i = 0; i < blockedListIndex; i++) { // after getting all blocked ones added them into end of queue and toggle state to ready as well
			pqueue_append(queue, blockedList[i]);
			os_getProcessSlot(blockedList[i])->state = OS_PS_READY;
		}
		blockedListIndex = 0; // reset blocked-list for the next iteration
	}

    return nextPidInQueue;
}