#include "os_memory.h"
#include "os_memory_strategies.h"
#include "os_memheap_drivers.h"

MemAddr *lastAlloc = 0;
MemAddr lastAllocExt = 0;
MemAddr lastAllocInt = 0;

MemAddr os_Memory_FirstFit(Heap *heap, size_t size) {
    MemAddr currAdrr = heap->useStartingAddress;
    MemAddr useEndingAddress = heap->useStartingAddress + heap->useSize - 1;
    size_t currChunkSize = 0;

    while (currChunkSize < size) {
        if (currAdrr > useEndingAddress) return 0;

        if (os_getMapEntry(heap, currAdrr) == 0) {
            currChunkSize += 1;
        } else {
            currChunkSize = 0;
        }

        currAdrr += 1;
    }

    currAdrr -= size; // get back the beginning of chunk
    return currAdrr;
}

MemAddr os_Memory_NextFit (Heap *heap, size_t size) {
    if(heap==intHeap)lastAlloc=&lastAllocInt;
    else  lastAlloc=&lastAllocExt;
    if(*lastAlloc==0) *lastAlloc=heap->useStartingAddress;
    MemAddr currAdrr = *lastAlloc;
    MemAddr useEndingAddress = heap->useStartingAddress + heap->useSize - 1;
    size_t  currChunkSize = 0;
    uint8_t checkedEverything=0;

    while (currChunkSize < size) {
        if(currAdrr==useEndingAddress+1 && *lastAlloc > heap->useStartingAddress && checkedEverything==0){
            currAdrr=heap->useStartingAddress;
            checkedEverything=1;
            currChunkSize = 0;
        }
        if (currAdrr==useEndingAddress+1) return 0;
        if (os_getMapEntry(heap, currAdrr) == 0) {
            currChunkSize += 1;
        } else {
            currChunkSize = 0;
        }
        currAdrr += 1;
    }

    *lastAlloc=currAdrr;
    currAdrr -= size; // get back to the beginning of chunk
    return currAdrr;

}




MemAddr os_Memory_BestFit(Heap *heap, size_t size) {
    MemAddr currAdrr = heap->useStartingAddress;
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    MemAddr bestFitAddr = 0;
    size_t currChunkSize;
    size_t minDiffChunkSize = SIZE_MAX;

    while (currAdrr <= useEndAddress) {
        currChunkSize = os_getChunkSizeNonZero(heap, currAdrr);
        if (os_getMapEntry(heap, currAdrr) == 0 && currChunkSize >= size) {
            if (currChunkSize - size < minDiffChunkSize) {
                minDiffChunkSize = currChunkSize - size;
                bestFitAddr = currAdrr;
            }
        }
        currAdrr += currChunkSize;
    }
    return bestFitAddr;
}

MemAddr os_Memory_WorstFit(Heap *heap, size_t size) {
    MemAddr currAdrr = heap->useStartingAddress;
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    MemAddr worstFitAddr = 0;
    size_t currChunkSize;
    size_t maxDiffChunkSize = 0;

    while (currAdrr <= useEndAddress) {
        currChunkSize = os_getChunkSizeNonZero(heap, currAdrr);
        if (os_getMapEntry(heap, currAdrr) == 0 && currChunkSize >= size) {
            if (currChunkSize - size >= maxDiffChunkSize) {
                maxDiffChunkSize = currChunkSize - size;
                worstFitAddr = currAdrr;
            }
        }
        currAdrr += currChunkSize;
    }
    return worstFitAddr;
}